import { BuildersLoader } from '../../src/default/builder.loader';
import { BuilderRegistry } from '../../src/builder.registry';

describe('nest-app-builder', () => {

  const builderRegistry: BuilderRegistry = BuilderRegistry.instance();

  it('Load All Builders', async () => {
    expect(await BuildersLoader.loadAllBuilders(builderRegistry)).toEqual(true);
  });

});

import { BuilderRegistry } from '../../src/builder.registry';
import { SwaggerBuilder } from '../../src/default/swagger.builder';
import { NestFactory } from '@nestjs/core';
import { AppModule } from '../app.module';

describe('nest-app-builder', () => {

  const builderRegistry: BuilderRegistry = BuilderRegistry.instance()
  const swaggerBuilder = SwaggerBuilder.instance(builderRegistry);

  it('Load All Builders', async () => {
    const app = await NestFactory.create(AppModule);
    expect(await swaggerBuilder.build(app, {})).toEqual(true);
  });

});

import { json } from 'body-parser';
import { IBuilder } from '../builder.interface';
import { BuilderRegistry } from '../builder.registry';
import { INestApplication } from '@nestjs/common';

export class InterceptorBuilder implements IBuilder {

  public static instance(builderRegistry: BuilderRegistry): IBuilder {
    if (this.thisObject) {
      return this.thisObject;
    }
    this.thisObject = new InterceptorBuilder();
    builderRegistry.registerBuilder(this.thisObject);
    return this.thisObject;
  }

  private static thisObject: InterceptorBuilder;
  private constructor() { }

  public build(app: INestApplication) {
    app.use(json({ limit: '50mb' }));
  }
}

import { BuilderRegistry } from '../builder.registry';
import { InterceptorBuilder } from './interceptor.builder';
import { SwaggerBuilder } from './swagger.builder';

export class BuildersLoader {

  public static loadAllBuilders(builderRegistry: BuilderRegistry) {
    SwaggerBuilder.instance(builderRegistry);
    InterceptorBuilder.instance(builderRegistry);
    return true;
  }
  private constructor() {}
}
